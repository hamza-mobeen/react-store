import React, { PureComponent } from 'react';

import Person from './Person/Person';
import axios from 'axios';

class Persons extends PureComponent {
  state ={
    posts:[]
  }
  
componentDidMount(){

  axios.get('https://jsonplaceholder.typicode.com/posts')
  .then(response => {
    this.setState({posts: response.data})

    console.log(response);
  })
}

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log('[Persons.js] getSnapshotBeforeUpdate');
    return { message: 'Snapshot!' };
  }

  
  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('[Persons.js] componentDidUpdate');
    console.log(snapshot);
  }

  componentWillUnmount() {
    console.log('[Persons.js] componentWillUnmount');
  }

  render() {

    const posts = this.state.posts.map(post => {
      return <Person key={post.id} name={post.title}/>
    })
    console.log('[Persons.js] rendering...');
    return this.props.persons.map((person, index) => {
      return (
        <Person
          click={() => this.props.clicked(index)}
          name={person.name}
          age={person.age}
          key={person.id}
          changed={event => this.props.changed(event, person.id)}
          isAuth={this.props.isAuthenticated}
        />
      );
    });
  
  }
}

export default Persons;
