import React , {Component, createRef, useEffect, useRef} from 'react';
import Auxiliary from '../../../HOC/Auxiliary';
import AuthContext from '../../../Context/auth-context';
//import withClass from '../HOC/withClass';
import  PropTypes from 'prop-types';

class Person extends Component { 
constructor(props){
    super(props);
    this.toggleInputRef = React.createRef();
}

static contextType = AuthContext;
componentDidMount(){
    console.log(this.context.authenticated);
}
render(){

    return (
        <Auxiliary>
            
            {this.context.authenticated ? <p>Authenticated</p>: <p>please Log in</p>}
            
           
            <p onClick={this.props.click}>I'm {this.props.name} and I am {this.props.age} years old!</p>
            <p>{this.props.children}</p>
            <input type="text" ref={this.toggleInputRef} onChange={this.props.changed}  />
        </Auxiliary>
    )

}
}

Person.propTypes = {
    name :PropTypes.string,
    age : PropTypes.number,
    click : PropTypes.func,
    changed: PropTypes.func
}

export default Person;